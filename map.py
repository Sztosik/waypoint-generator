import folium

def add_markers(points, map):
    for point in points:
        folium.Marker(location=[point[0], point[1]], popup=(point[0],point[1]), tooltip=point[2]).add_to(map)
    return map

def draw_lines(points, map, line_color):
    points_list = []
    for point in points:
        points_list.append((point[0], point[1]))

    folium.PolyLine(points_list, color=line_color, weight=2.5, opacity=1).add_to(map)
    return map

def area_points(points_list):
    area = []
    reference_point = points_list[0]
    for point in points_list:
        area.append((point.lat, point.lon))
    area.append((reference_point.lat, reference_point.lon))
    return area


