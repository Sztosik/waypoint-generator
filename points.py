from dataclasses import dataclass

@dataclass
class Points():
    number: int = 0
    lat: float = 0.0
    lon: float = 0.0
    n: float = 0.0
    e: float = 0.0
    d: float = 0.0
