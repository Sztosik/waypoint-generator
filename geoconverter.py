import math
import config


def generate_waypoints(points_list):
    
    waypoints = []
    reference_point = points_list[0]
    lines, min_north, max_north = find_linear_equations(points_list)

    i=1
    y = max_north - (config.FILLING/2)
    while y > min_north:
        for line in lines:
            a, b, x1, x2, y1, y2 = line

            if (y1 < y  and y < y2) or (y1 > y  and y > y2):
                x = (y - b) / a
                if (x1 < x  and x < x2) or (x1 > x  and x > x2):
                    lat = north2lat(y, reference_point.lat)
                    lon = east2lon(x, reference_point.lat, reference_point.lon)
                    waypoints.append((lat, lon, i))
                    i+=1
        y = y - config.FILLING
    return sort_points(waypoints)


def convert2cartesian(points_list):
    reference_point = points_list[0]
    for point in points_list:
        if point == reference_point:
            continue
        point.n = lat2north(reference_point.lat, point.lat)
        point.e = lon2east(reference_point.lat, reference_point.lon, point.lon)


def find_linear_equations(points_list):
    lines = []
    max_north = 0.0
    min_north = 0.0

    points_list.append(points_list[0])

    i=1
    while i<len(points_list):
        y1 = points_list[i-1].n
        x1 = points_list[i-1].e
        y2 = points_list[i].n
        x2 = points_list[i].e

        if(y1 > max_north):
            max_north = y1

        if(y1 < min_north):
            min_north = y1

        a = (y2 - y1)/(x2 - x1)
        b = y1-(a*x1)

        lines.append((a,b,x1,x2,y1,y2))
        i+=1
    return lines, min_north, max_north


def lat2north(lat1, lat2):
    return (lat2 - lat1)*(40075704.0/360)


def north2lat(n, lat1):
    return (n / (40075704.0/360)) + lat1


def lon2east(lat1, lon1, lon2):
    lat1 = math.radians(lat1)
    return math.cos(lat1)*(lon2 - lon1)*(40075704.0/360)


def east2lon(e, lat1, lon1):
    lat1 = math.radians(lat1)
    return (e/(math.cos(lat1)*(40075704.0/360)))+lon1


def sort_points(points):
    numbered_points = []
    i=0
    # sorting loop
    for point in points:
        if point[2]%4 == 0:
            temp = points[i-1]
            points[i-1] = points[i]
            points[i] = temp
        i+=1
    # numbering loop
    i=1
    for point in points:
        numbered_points.append((point[0], point[1], i))
        i+=1
    return numbered_points

