# Waypoint Creator

## Technologies
Project is created with:
* folium version: 0.12.1

## Setup
__To run this project, create virtual environment and install requirements__

```
$ python -m venv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```
__Run main__ 

```
$ python main.py
```
__Parameter n is the distance between successive flights expressed in meters__

```
waypoints = geoconverter.generate_waypoints(points_list, n)
```
__geojson.io__

## Overview map of the route in the index.html file
![Example screenshot](./images/screenshot.png) 