import json

def save_json(points_list):
    POINTS = {
        "type": "WaypointsCollection",
        "waypoints": []
    }
    
    for coordinate in points_list:
        lat, lon, number = coordinate
        POINT = {
            "type": "Point",
            "number": number,
            "coordinates": [lat, lon]
        }
        POINTS["waypoints"].append(POINT)

    with open('waypoints.json', 'w') as f:
        json.dump(POINTS, f, indent=2)
