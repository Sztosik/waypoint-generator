import folium
import geoconverter
import map
import list2json
import json
from points import Points

"""
Droniada
52.238949 16.239416
52.238207 16.243638
52.235602 16.242536
52.236311 16.238301
"""

with open('data.json', 'r') as file:
    geojson = json.load(file)
json_list = geojson["features"][0]["geometry"]["coordinates"][0]

points_list = [] # points object list

i = 1
for coordinates in json_list:
    longitude, latitude = coordinates
    point = Points(number=i, lat=latitude, lon=longitude)
    points_list.append(point)
    i+=1

geoconverter.convert2cartesian(points_list)
waypoints = geoconverter.generate_waypoints(points_list)
list2json.save_json(waypoints)



# GENERATE MAP
reference_point = points_list[0]
center = [reference_point.lat, reference_point.lon]
m = folium.Map(location=center, zoom_start=17)

# Open Street Map Layer
folium.raster_layers.TileLayer('Open Street Map').add_to(m)

# Google Satellite Layer
folium.raster_layers.TileLayer(
        tiles = 'https://mt1.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',
        attr = 'Google',
        name = 'Google Satellite',
        overlay = False,
        control = True
    ).add_to(m)

# Google Maps Layer
folium.raster_layers.TileLayer(
        tiles = 'https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',
        attr = 'Google',
        name = 'Google Maps',
        overlay = False,
        control = True
    ).add_to(m)

folium.LayerControl().add_to(m)

area = map.area_points(points_list)
m = map.draw_lines(area, m, "red")

# m = map.add_markers(waypoints, m)
m = map.draw_lines(waypoints, m, "blue")

m.save('index.html')
print("done")
